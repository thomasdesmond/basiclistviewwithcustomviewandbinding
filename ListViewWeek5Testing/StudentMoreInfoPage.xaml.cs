﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ListViewWeek5Testing
{
    public partial class StudentMoreInfoPage : ContentPage
    {
        public StudentMoreInfoPage()
        {
            InitializeComponent();
        }

        public StudentMoreInfoPage(Student student)
        {
            InitializeComponent();

            BindingContext = student;
        }
    }
}
