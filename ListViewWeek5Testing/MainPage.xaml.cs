﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListViewWeek5Testing
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            PopulateStudentListView();
        }

        private void PopulateStudentListView()
        {
            var studentList = new ObservableCollection<Student>();

            var student1 = new Student
            {
                Name = "Thomas",
                Class = "CS481",
                Grade = "A",
                MoreInfo = "Student is the best",
            };

            var student2 = new Student
            {
                Name = "Dustin",
                Class = "CS544",
                Grade = "B",
                MoreInfo = "Not the best",
            };

            studentList.Add(student1);
            studentList.Add(student2);


            StudentListView.ItemsSource = studentList;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var student = (Student)menuItem.CommandParameter;
            Navigation.PushAsync(new StudentMoreInfoPage(student));
        }
    }
}
