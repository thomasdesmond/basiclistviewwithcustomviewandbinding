﻿using System;
namespace ListViewWeek5Testing
{
    public class Student
    {
        public string Name
        {
            get;
            set;
        }

        public string Class
        {
            get;
            set;
        }

        public string Grade
        {
            get;
            set;
        }

        public string MoreInfo
        {
            get;
            set;
        }
    }
}
